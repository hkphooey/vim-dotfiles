## Clone

    git clone --recursive https://bitbucket.org/hkphooey/vim-dotfiles.git ~/.vim
	ln -s ~/.vim/.vimrc ~/.vimrc

## Clone missing submodules

    git submodule update --init --recursive

## Update

    git pull --recurse-submodules
